#    ______  _____  _    _______  ______  _____    _____ 
#   |  ____||_   _|| |  |__   __||  ____||  __ \  / ____|
#   | |__     | |  | |     | |   | |__   | |__) || (___  
#   |  __|    | |  | |     | |   |  __|  |  _  /  \___ \ 
#   | |      _| |_ | |____ | |   | |____ | | \ \  ____) |
#   |_|     |_____||______||_|   |______||_|  \_\|_____/ 
# 
#   ---------------------------------------
#   Filter functions
#
#   Author : Corcos Ludovic

import numpy as np
from skimage import exposure, filters, segmentation
from scipy import ndimage as ndi

# Sobel filter
def sobel2D(image):
    sx = ndi.sobel(image, axis=0, mode='reflect')
    sy = ndi.sobel(image, axis=1, mode='reflect')
    sob = np.hypot(sx, sy)
    return sob

# Sobel filter
def sobel3D(image):
    sx = ndi.sobel(image, axis=0, mode='reflect')
    sy = ndi.sobel(image, axis=1, mode='reflect')
    sz = ndi.sobel(image, axis=2, mode='reflect')
    sob = np.hypot(sx, sy, sz)
    return sob

def removeBackground(image, file):
    size = len(image)
    
    for i in range(0,size):
        
        img = image[i]
        
        elevation_map = sobel2D(img)  # Sobel filter
        
        markers = np.zeros_like(img)
        if 'T2' in file:
            markers[img < 1] = 1
            markers[img > 60] = 2
        else:
            markers[img < 1] = 1
            markers[img > 120] = 2
            
        segmentation_base = segmentation.watershed(elevation_map, markers, watershed_line = True)

        segmentation_best_outline = ndi.median_filter(segmentation_base, 20)

        segmentation_final = ndi.binary_fill_holes(segmentation_best_outline - 1)

        mask, _ = ndi.label(segmentation_final)

        img[mask == 0] = 0
        
        image[i] = img
        
    return image