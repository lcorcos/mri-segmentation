#     _____  ____   _   _  ______  _____  _____ 
#    / ____|/ __ \ | \ | ||  ____||_   _|/ ____|
#   | |    | |  | ||  \| || |__     | | | |  __ 
#   | |    | |  | || . ` ||  __|    | | | | |_ |
#   | |____| |__| || |\  || |      _| |_| |__| |
#    \_____|\____/ |_| \_||_|     |_____|\_____|
# 
#   ---------------------------------------
#   Some general information
#
#   Author : Corcos Ludovic


DATA_PATH = '/user/lcorcos/home/Desktop/MRI_Project/Data/'

DEFAULT_NAME = "NC"

VERSION = 1.0

SAVE_FIGS = True

MODULES = ['tensorflow', 'tensorflow.keras', 'numpy', 'sklearn',
           'skimage', 'scikit-image', 'matplotlib', 'plotly', 'pandas', 'jupyterlab', 'nibabel']
