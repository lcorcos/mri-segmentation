#    _____  _____   _____  _____   _             __     __
#   |  __ \|_   _| / ____||  __ \ | |         /\ \ \   / /
#   | |  | | | |  | (___  | |__) || |        /  \ \ \_/ / 
#   | |  | | | |   \___ \ |  ___/ | |       / /\ \ \   /  
#   | |__| |_| |_  ____) || |     | |____  / ____ \ | |   
#   |_____/|_____||_____/ |_|     |______|/_/    \_\|_|   
#
#   ---------------------------------------
#   Display funtions
#
#   Author : Corcos Ludovic

import itertools
import math
import scipy.ndimage as ndi
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider
from nilearn.plotting import view_img
from sklearn.metrics import confusion_matrix

def displayMRI(img, title):
    colormap = 'afmhot'
    return view_img(img, cmap=colormap, black_bg=False, symmetric_cmap=False, title = title)

def displayMRIplot(file, init_slice, slider = False, title = ''):
    colormap = 'afmhot'
    
    dim = file.shape
    
    fig = plt.figure(figsize=(16, 5))
    gs = fig.add_gridspec(1, 3, hspace=0, wspace=0)
    (sagital, axial, coronal) = gs.subplots()
    fig.suptitle(title, fontsize=16)

    sagital.set_title("Sagittal", fontsize=11)
    sagital.axis('off')
    image_s = file[init_slice, :, :]
    sagital.imshow(image_s, cmap=colormap, interpolation='nearest')
    
    coronal.set_title("Coronal", fontsize=11)
    coronal.axis('off')
    image_c = file[:, init_slice, :]
    coronal.imshow(image_c, cmap=colormap, interpolation='nearest')

    axial.set_title("Axial", fontsize=11)
    axial.axis('off')
    image_a = file[:, :, init_slice]
    axial.imshow(image_a, cmap=colormap, interpolation='nearest')

    if slider:
        ax_slider_sagital = plt.axes([0.15, 0.05, 0.2, 0.03])
        sagital_slider = Slider(ax_slider_sagital, '', 0, dim[0] - 1, valinit=init_slice, valstep=1)

        ax_slider_coronal = plt.axes([0.67, 0.05, 0.2, 0.03])
        coronal_slider = Slider(ax_slider_coronal, '', 0, dim[1] - 1, valinit=init_slice, valstep=1)
        
        ax_slider_axial = plt.axes([0.41, 0.05, 0.2, 0.03])
        
        axial_slider = Slider(ax_slider_axial, '', 0, dim[2] - 1, valinit=init_slice, valstep=1)

        def update_S(val):
            image_s = file[int(sagital_slider.val), :, :]
            sagital.imshow(image_s, cmap=colormap, interpolation='nearest')
            fig.canvas.flush_events()
            fig.canvas.draw_idle()
            
        def update_C(val):
            image_c = file[:, int(coronal_slider.val), :]
            coronal.imshow(image_c, cmap=colormap, interpolation='nearest')
            fig.canvas.flush_events()
            fig.canvas.draw_idl()
            
        def update_A(val):
            image_a = file[:, :, int(axial_slider.val)]
            axial.imshow(image_a, cmap=colormap, interpolation='nearest')
            fig.canvas.flush_events()
            fig.canvas.draw_idle()
        
        sagital_slider.on_changed(update_S)
        coronal_slider.on_changed(update_C)
        axial_slider.on_changed(update_A)

    plt.axis('off')
    plt.show()

def displayMRIplotMask(file, mask, init_slice, slider = False, title = ''):
    colormap = 'afmhot'

    dim = file.shape
    
    fig = plt.figure(figsize=(16, 5))
    gs = fig.add_gridspec(1, 3, hspace=0, wspace=0)
    (sagital, axial, coronal) = gs.subplots()
    fig.suptitle(title, fontsize=16)

    # Sagittal plot
    sagital.set_title("Sagittal", fontsize=11)
    sagital.axis('off')
    image_s = file[init_slice, :, :]
    sagital.imshow(image_s, cmap=colormap, interpolation='nearest')
    m_s = mask[init_slice, :, :]
    sagital.imshow(m_s, interpolation='nearest', alpha=0.5)

    # Coronal plot
    coronal.set_title("Coronal", fontsize=11)
    coronal.axis('off')
    image_c = file[:, init_slice, :]
    coronal.imshow(image_c, cmap=colormap, interpolation='nearest')
    m_c = mask[:, init_slice, :]
    coronal.imshow(m_c, interpolation='nearest', alpha=0.5)

    # Axial plot
    axial.set_title("Axial", fontsize=11)
    axial.axis('off')
    image_a = file[:, :, init_slice]
    axial.imshow(image_a, cmap=colormap, interpolation='nearest')
    m_a = mask[:, :, init_slice]
    axial.imshow(m_a, interpolation='nearest', alpha=0.5)

    if slider:
        ax_slider_sagital = plt.axes([0.15, 0.05, 0.2, 0.03])
        sagital_slider = Slider(ax_slider_sagital, '', 0, dim[0] - 1, valinit=init_slice, valstep=1)

        ax_slider_coronal = plt.axes([0.67, 0.05, 0.2, 0.03])
        coronal_slider = Slider(ax_slider_coronal, '', 0, dim[1] - 1, valinit=init_slice, valstep=1)
        
        ax_slider_axial = plt.axes([0.41, 0.05, 0.2, 0.03])
    
        axial_slider = Slider(ax_slider_axial, '', 0, dim[2] - 1, valinit=init_slice, valstep=1)

        def update_S(val):
            image_s = file[int(sagital_slider.val), :, :]
            sagital.imshow(image_s, cmap=colormap, interpolation='nearest')
            m_s = mask[int(sagital_slider.val), :, :]
            sagital.imshow(m_s, interpolation='nearest', alpha=0.5)
            fig.canvas.flush_events()
            fig.canvas.draw_idle()
            
        def update_C(val):
            image_c = file[:, int(coronal_slider.val), :]
            coronal.imshow(image_c, cmap=colormap, interpolation='nearest')
            m_c = mask[:, int(coronal_slider.val), :]
            coronal.imshow(m_c, interpolation='nearest', alpha=0.5)
            fig.canvas.flush_events()
            fig.canvas.draw_idl()
            
        def update_A(val):
            image_a = file[:, :, int(axial_slider.val)]
            axial.imshow(image_a, cmap=colormap, interpolation='nearest')
            m_a = mask[:, :, int(axial_slider.val)]
            axial.imshow(m_a, interpolation='nearest', alpha=0.5)
            fig.canvas.flush_events()
            fig.canvas.draw_idle()
        
        sagital_slider.on_changed(update_S)
        coronal_slider.on_changed(update_C)
        axial_slider.on_changed(update_A)

    plt.axis('off')
    plt.show()
    
def montageSix(nii_file, title):
    dim = nii_file.shape

    Position = range(1, 7)

    colormap = 'afmhot'
    fig = plt.figure(figsize=(30, 5))
    plt.title(title, size=20, y=1.1)
    plt.text(0.148,1.05,'Axial', size=15)
    plt.text(0.483,1,'Coronal', size=15)
    plt.text(0.827,1,'Sagital', size=15)
    plt.axis('off')
    for k in range(6):

        ax = fig.add_subplot(1, 6, Position[k])

        if k < 2:
            slice = (k + 1) * int(dim[0]/3)
            image = nii_file[slice, :, :]
            ax.imshow(image, cmap=colormap, interpolation='None')
            ax.set_title('Slice ' + str(slice), y=-0.1)

        elif k >= 2 and k  <= 3:
            slice = (k - 1) * int(dim[1]/3)
            image = nii_file[:, slice, :]
            ax.imshow(image, cmap=colormap, interpolation='None')
            ax.set_title('Slice ' + str(slice), y=-0.1)
            
        else:
            slice = (k - 3) * int(dim[2]/3)
            image = nii_file[:, :, slice]
            ax.imshow(image, cmap=colormap, interpolation='None')
            ax.set_title('Slice ' + str(slice), y=-0.1)

        ax.set_axis_off()

    plt.show()

def compareTwo(img1, title1, img2, title2):
    montageSix(img1, title1)
    montageSix(img2, title2)
    
def compareThree(img1, title1, img2, title2, img3, title3):
    montageSix(img1, title1)
    montageSix(img2, title2)
    montageSix(img3, title3)