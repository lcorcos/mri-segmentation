import sys
import scipy.ndimage as ndi
import nibabel as nib
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider

def displayMRIplot(path, init_slice, slider = False, title = 'MRI'):
    colormap = 'afmhot'

    nii_img = nib.load(f'{path}')
    file = nii_img.get_fdata()
    dim = file.shape
    
    fig = plt.figure(figsize=(16, 5))
    gs = fig.add_gridspec(1, 3, hspace=0, wspace=0)
    (axial, sagital, coronal) = gs.subplots()
    fig.suptitle(title, fontsize=16)

    sagital.set_title("Sagittal", fontsize=11)
    sagital.axis('off')
    image_s = file[init_slice, :, :]
    sagital.imshow(ndi.rotate(image_s,90), cmap=colormap, interpolation='none')
    sagital.imshow(ndi.rotate(image_s,90), cmap=colormap, interpolation='none')

    coronal.set_title("Coronal", fontsize=11)
    coronal.axis('off')
    image_c = file[:, init_slice, :]
    coronal.imshow(ndi.rotate(image_c,90) , cmap=colormap, interpolation='none')

    axial.set_title("Axial", fontsize=11)
    axial.axis('off')
    image_a = file[:, :, init_slice]
    axial.imshow(ndi.rotate(image_a,90), cmap=colormap, interpolation='none')

    if slider:
        ax_slider_sagital = plt.axes([0.41, 0.05, 0.2, 0.03])
        sagital_slider = Slider(ax_slider_sagital, '', 0, dim[0] - 1, valinit=init_slice, valstep=1)

        ax_slider_coronal = plt.axes([0.67, 0.05, 0.2, 0.03])
        coronal_slider = Slider(ax_slider_coronal, '', 0, dim[1] - 1, valinit=init_slice, valstep=1)
        
        ax_slider_axial = plt.axes([0.15, 0.05, 0.2, 0.03])
        axial_slider = Slider(ax_slider_axial, '', 0, dim[2] - 1, valinit=init_slice, valstep=1)

        def update_S(val):
            image_s = file[int(sagital_slider.val), :, :]
            sagital.imshow(ndi.rotate(image_s,90), cmap=colormap, interpolation='none')
            fig.canvas.draw_idle()
            
        def update_C(val):
            image_c = file[:, int(coronal_slider.val), :]
            coronal.imshow(ndi.rotate(image_c,90), cmap=colormap, interpolation='none')
            fig.canvas.draw_idle()
            
        def update_A(val):
            image_a = file[:, :, int(axial_slider.val)]
            axial.imshow(ndi.rotate(image_a,90), cmap=colormap, interpolation='none')
            fig.canvas.draw_idle()
        
        sagital_slider.on_changed(update_S)
        coronal_slider.on_changed(update_C)
        axial_slider.on_changed(update_A)

    plt.axis('off')
    plt.show()


file_path = sys.argv[1]

displayMRIplot(file_path, 100, True, file_path)