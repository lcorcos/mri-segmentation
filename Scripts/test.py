import argparse
import sys
import numpy as np
from skimage import segmentation
from scipy import ndimage as ndi
import nibabel as nib
from os import system
import ntpath


# ─── UTILITY ────────────────────────────────────────────────────────────────────

class colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def progressbar(it, prefix="", size=60, file=sys.stdout):
    count = len(it)
    def show(j):
        x = int(size*j/count)
        file.write("{}[{}{}] {} / {}\r".format(prefix, "█"*x, "."*(size-x), j, count))
        file.flush()
    show(0)
    for i, item in enumerate(it):
        yield item
        show(i+1)
    file.flush()
    file.write("\n")

# ─── CLASSES FOR ARGUMENT MANAGEMENT ────────────────────────────────────────────


class HelpFormatter(argparse.HelpFormatter):
    def add_usage(self, usage, actions, groups, prefix=None):
        if prefix is None:
            prefix = colors.BOLD + colors.UNDERLINE + 'Usage:' + colors.ENDC + ' '
        return super(HelpFormatter, self).add_usage(
            usage, actions, groups, prefix)


class MyArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        self.print_usage()
        str = colors.BOLD + colors.FAIL + 'ERROR: ' + message + '\n' + colors.ENDC
        self.exit(22, str)

# ─── ARGUMENT MANAGEMENT ────────────────────────────────────────────────────────


parser = MyArgumentParser(add_help=False,
                          formatter_class=HelpFormatter,
                          description=colors.BOLD + colors.UNDERLINE + colors.OKCYAN + "Background suppression in brain MRI images (T1 - T2)" + colors.ENDC)

parser._positionals.title = colors.BOLD + \
    colors.UNDERLINE + 'Positional arguments' + colors.ENDC
parser._optionals.title = colors.BOLD + \
    colors.UNDERLINE + 'Optional arguments' + colors.ENDC

parser.add_argument('-i', '--input',
                    type=str,
                    required=True,
                    help="Link to *.nii.gz or *.nii file where background must be removed. Ex: file_in.nii.gz")

parser.add_argument('-m', '--modality',
                    required=True,
                    choices=['T1', 'T2'],
                    help="MRI contrast modality")

parser.add_argument('-o', '--output',
                    type=str,
                    required=True,
                    help="Link to the output file : result of the export, a new Nifti image. Ex: file_out.nii.gz")

parser.add_argument('-v', '--version', action='version',
                    version='%(prog)s 1.0', help="Show program's version number and exit.")

parser.add_argument('-h', '--help', action='help',
                    default=argparse.SUPPRESS, help='Show this help message and exit.')

args = parser.parse_args()

# ─── FUNCTIONS FOR MRI PROCESSING ───────────────────────────────────────────────


def sobel2D(image):
    sx = ndi.sobel(image, axis=0, mode='reflect')
    sy = ndi.sobel(image, axis=1, mode='reflect')
    sob = np.hypot(sx, sy)
    return sob


def removeBackground(image, mod, file_name):
    size = len(image)

    for i in progressbar(range(size), "File: " + file_name + " ", 70):

        img = image[i]

        elevation_map = sobel2D(img)

        markers = np.zeros_like(img)
        if mod == 'T2':
            markers[img < 1] = 1
            markers[img > 60] = 2
        else:
            markers[img < 1] = 1
            markers[img > 120] = 2

        segmentation_base = segmentation.watershed(
            elevation_map, markers, watershed_line=True)

        segmentation_best_outline = ndi.median_filter(segmentation_base, 20)

        segmentation_final = ndi.binary_fill_holes(
            segmentation_best_outline - 1)

        mask, _ = ndi.label(segmentation_final)

        img[mask == 0] = 0

        image[i] = img

    return image

# ─── MAIN SCRIPT ────────────────────────────────────────────────────────────────


system('clear')

input_path = args.input
out_path = args.output
mod = args.modality

# Nii process
nii_img = nib.load(input_path)
nii_data = nii_img.get_fdata()
nii_data = np.rot90(nii_data, 1, axes=(0, 2))  # ! Important

file_name = ntpath.basename(input_path)
result_ndarray = removeBackground(nii_data, mod, mod + " - " + file_name)

print("Saving in progress...")
nib.save(nib.Nifti1Image(result_ndarray, nii_img.affine), out_path)
