#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import nibabel as nib
from os import system
import ants

# ─── UTILITY ────────────────────────────────────────────────────────────────────

class colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# ─── CLASSES FOR ARGUMENT MANAGEMENT ────────────────────────────────────────────

class HelpFormatter(argparse.HelpFormatter):
    def add_usage(self, usage, actions, groups, prefix=None):
        if prefix is None:
            prefix = colors.BOLD + colors.UNDERLINE + 'Usage:' + colors.ENDC + ' '
        return super(HelpFormatter, self).add_usage(
            usage, actions, groups, prefix)


class MyArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        self.print_usage()
        str = colors.BOLD + colors.FAIL + 'ERROR: ' + message + '\n' + colors.ENDC
        self.exit(22, str)

# ─── ARGUMENT MANAGEMENT ────────────────────────────────────────────────────────

parser = MyArgumentParser(add_help=False,
                          formatter_class=HelpFormatter,
                          description=colors.BOLD + colors.UNDERLINE + colors.OKCYAN + "Affine registration in brain MRI images (T1 - T2)" + colors.ENDC)

parser._positionals.title = colors.BOLD + \
    colors.UNDERLINE + 'Positional arguments' + colors.ENDC
parser._optionals.title = colors.BOLD + \
    colors.UNDERLINE + 'Optional arguments' + colors.ENDC

parser.add_argument('-s', '--source',
                    type=str,
                    required=True,
                    help="Source file (*.nii.gz or *.nii) corresponding to the original MRI file.")

parser.add_argument('-i', '--input',
                    type=str,
                    required=True,
                    help="Link to *.nii.gz or *.nii file corresponding to the manual segmented mask. Ex: file_in.nii.gz")

parser.add_argument('-mat', '--mat_file_name',
                    type=str,
                    required=True,
                    help="Filename corresponding to the associated transformation matrix.")

parser.add_argument('-inv', '--inversion_file',
                    type=str,
                    required=True,
                    help="Filename corresponding to the associated inversion file.")

parser.add_argument('-o', '--output',
                    type=str,
                    required=True,
                    help="Link to the output file : result of the export, a new Nifti image. Ex: file_out.nii.gz")

parser.add_argument('-v', '--version', action='version',
                    version='%(prog)s 1.0', help="Show program's version number and exit.")

parser.add_argument('-h', '--help', action='help',
                    default=argparse.SUPPRESS, help='Show this help message and exit.')

args = parser.parse_args()

# ─── MAIN SCRIPT ────────────────────────────────────────────────────────────────

system('clear')

print("Start")

source_file = args.source
input_path = args.input
mat_name = args.mat_file_name
inv_name = args.inversion_file
out_path = args.output


# Nii process
nii_origin = nib.load(input_path)

source_file = ants.image_read(source_file)
input_path = ants.image_read(input_path)

tlist = [mat_name,inv_name]

mywarpedimage = ants.apply_transforms(fixed=source_file, moving=input_path, transformlist=tlist, interpolator='genericLabel')

print("Saving in progress...")

nib.save(nib.Nifti1Image(mywarpedimage[:][:][:], nii_origin.affine), out_path)

