#!/bin/sh

python3 Semi_automated_segmentation.py \
-m /user/lcorcos/home/Desktop/MRI_Project/Data/Fast/100206/T1w/T1w_acpc_dc_restore.nii.gz \
-hs /user/lcorcos/home/Desktop/MRI_Project/Data/Inversed_Registration/100206/T1w/Manual_mask_registrated.nii.gz \
-o /user/lcorcos/home/Desktop/MRI_Project/Data/Masks/100206/T1w/Final_mask.nii.gz

python3 Semi_automated_segmentation.py \
-m /user/lcorcos/home/Desktop/MRI_Project/Data/Fast/100307/T1w/T1w_acpc_dc_restore.nii.gz \
-hs /user/lcorcos/home/Desktop/MRI_Project/Data/Inversed_Registration/100307/T1w/Manual_mask_registrated.nii.gz \
-o /user/lcorcos/home/Desktop/MRI_Project/Data/Masks/100307/T1w/Final_mask.nii.gz

python3 Semi_automated_segmentation.py \
-m /user/lcorcos/home/Desktop/MRI_Project/Data/Fast/101006/T1w/T1w_acpc_dc_restore.nii.gz \
-hs /user/lcorcos/home/Desktop/MRI_Project/Data/Inversed_Registration/101006/T1w/Manual_mask_registrated.nii.gz \
-o /user/lcorcos/home/Desktop/MRI_Project/Data/Masks/101006/T1w/Final_mask.nii.gz

python3 Semi_automated_segmentation.py \
-m /user/lcorcos/home/Desktop/MRI_Project/Data/Fast/101410/T1w/T1w_acpc_dc_restore.nii.gz \
-hs /user/lcorcos/home/Desktop/MRI_Project/Data/Inversed_Registration/101410/T1w/Manual_mask_registrated.nii.gz \
-o /user/lcorcos/home/Desktop/MRI_Project/Data/Masks/101410/T1w/Final_mask.nii.gz