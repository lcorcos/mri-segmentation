#!/bin/sh

python3 Inversed_Registration.py \
-s /user/lcorcos/home/Desktop/MRI_Project/Data/Best_Color/100206/T1w/T1w_acpc_dc_restore.nii.gz \
-i /user/lcorcos/home/Desktop/MRI_Project/Data/Manual_segmentation/Smooth_Mask_v6.nii.gz \
-mat /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/100206/T1w/100206_T1_0GenericAffine.mat \
-inv /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/100206/T1w/100206_T1_1InverseWarp.nii.gz \
-o /user/lcorcos/home/Desktop/MRI_Project/Data/Inversed_Registration/100206/T1w/Manual_mask_registrated.nii.gz

python3 Inversed_Registration.py \
-s /user/lcorcos/home/Desktop/MRI_Project/Data/Best_Color/100307/T1w/T1w_acpc_dc_restore.nii.gz \
-i /user/lcorcos/home/Desktop/MRI_Project/Data/Manual_segmentation/Smooth_Mask_v6.nii.gz \
-mat /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/100307/T1w/100307_T1_0GenericAffine.mat \
-inv /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/100307/T1w/100307_T1_1InverseWarp.nii.gz \
-o /user/lcorcos/home/Desktop/MRI_Project/Data/Inversed_Registration/100307/T1w/Manual_mask_registrated.nii.gz

python3 Inversed_Registration.py \
-s /user/lcorcos/home/Desktop/MRI_Project/Data/Best_Color/101006/T1w/T1w_acpc_dc_restore.nii.gz \
-i /user/lcorcos/home/Desktop/MRI_Project/Data/Manual_segmentation/Smooth_Mask_v6.nii.gz \
-mat /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/101006/T1w/101006_T1_0GenericAffine.mat \
-inv /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/101006/T1w/101006_T1_1InverseWarp.nii.gz \
-o /user/lcorcos/home/Desktop/MRI_Project/Data/Inversed_Registration/101006/T1w/Manual_mask_registrated.nii.gz