#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import sys
import numpy as np
from skimage import segmentation
from scipy import ndimage as ndi
import nibabel as nib
from os import system
import ntpath
from skimage import exposure
from skimage.filters.rank import median
from skimage.morphology import ball
from skimage.filters import unsharp_mask
from skimage.util import img_as_ubyte

# ─── UTILITY ────────────────────────────────────────────────────────────────────

class colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def progressbar(it, prefix="", size=60, file=sys.stdout):
    count = len(it)
    def show(j):
        x = int(size*j/count)
        file.write("{}[{}{}] {} / {}\r".format(prefix, "█"*x, "."*(size-x), j, count))
        file.flush()
    show(0)
    for i, item in enumerate(it):
        yield item
        show(i+1)
    file.flush()
    file.write("\n")

# ─── CLASSES FOR ARGUMENT MANAGEMENT ────────────────────────────────────────────

class HelpFormatter(argparse.HelpFormatter):
    def add_usage(self, usage, actions, groups, prefix=None):
        if prefix is None:
            prefix = colors.BOLD + colors.UNDERLINE + 'Usage:' + colors.ENDC + ' '
        return super(HelpFormatter, self).add_usage(
            usage, actions, groups, prefix)


class MyArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        self.print_usage()
        str = colors.BOLD + colors.FAIL + 'ERROR: ' + message + '\n' + colors.ENDC
        self.exit(22, str)

# ─── ARGUMENT MANAGEMENT ────────────────────────────────────────────────────────

parser = MyArgumentParser(add_help=False,
                          formatter_class=HelpFormatter,
                          description=colors.BOLD + colors.UNDERLINE + colors.OKCYAN + "Color enhancement in brain MRI images (T1 - T2)" + colors.ENDC)

parser._positionals.title = colors.BOLD + \
    colors.UNDERLINE + 'Positional arguments' + colors.ENDC
parser._optionals.title = colors.BOLD + \
    colors.UNDERLINE + 'Optional arguments' + colors.ENDC

parser.add_argument('-i', '--input',
                    type=str,
                    required=True,
                    help="Link to *.nii.gz or *.nii file where background is removed. Ex: file_in.nii.gz")

parser.add_argument('-m', '--modality',
                    required=True,
                    choices=['T1', 'T2'],
                    help="MRI contrast modality")

parser.add_argument('-o', '--output',
                    type=str,
                    required=True,
                    help="Link to the output file : result of the export, a new Nifti image where the intensities are improved. Ex: file_out.nii.gz")

parser.add_argument('-v', '--version', action='version',
                    version='%(prog)s 1.0', help="Show program's version number and exit.")

parser.add_argument('-h', '--help', action='help',
                    default=argparse.SUPPRESS, help='Show this help message and exit.')

args = parser.parse_args()

# ─── FUNCTIONS FOR MRI PROCESSING ───────────────────────────────────────────────

# Normalization function
def NormalizeData(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))

# T1 modal image processing
def T1_process(nii_image):
    # Rescale image intensity
    nii_data_rescale = exposure.rescale_intensity(nii_image)

    # Edge improvement
    nii_data_edge = unsharp_mask(nii_data_rescale, radius=1, amount=1)

    # Histogram equalization
    nii_equal_hist = exposure.equalize_hist(nii_data_edge)

    # Data normalization
    nii_data_norm = NormalizeData(nii_equal_hist)
    
    # Convert type
    nii_equal_hist = img_as_ubyte(nii_data_norm)

    # median filter
    smooth_nii = median(nii_equal_hist, ball(1.5))

    return smooth_nii

# T2 modal image processing
def T2_process(nii_image):

    # Rescale image intensity
    nii_data_rescale = exposure.rescale_intensity(nii_image)

    # Logarithmic correction 
    log_corrected = exposure.adjust_log(nii_data_rescale, 1.5)

    # Edge improvement
    nii_data_edge = unsharp_mask(log_corrected, radius=0.5, amount=0.5)

    # Data normalization
    nii_data_norm = NormalizeData(nii_data_edge)

    # Convert type
    nii_data_norm = img_as_ubyte(nii_data_norm)

    # Median filter
    smooth_nii = median(nii_data_norm, ball(1.7))

    return smooth_nii


# ─── MAIN SCRIPT ────────────────────────────────────────────────────────────────

system('clear')

input_path = args.input
out_path = args.output
mod = args.modality

# Nii process
nii_img = nib.load(input_path)
nii_data = nii_img.get_fdata()

print("Ongoing calculs...")
if mod == 'T1':
    result_ndarray = T1_process(nii_data)
else:
    result_ndarray = T2_process(nii_data)

print("Saving in progress...")
nib.save(nib.Nifti1Image(result_ndarray, nii_img.affine), out_path)
