#!/bin/sh

python3 Registration.py -s /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/101410/T1w/T1w_acpc_dc_restore.nii.gz \
-i /user/lcorcos/home/Desktop/MRI_Project/Data/Best_Color/100206/T1w/T1w_acpc_dc_restore.nii.gz \
-m T1 \
-o /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/100206/T1w/T1w_acpc_dc_restore.nii.gz \
-mat /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/100206/T1w/100206_T1_

python3 Registration.py -s /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/101410/T1w/T1w_acpc_dc_restore.nii.gz \
-i /user/lcorcos/home/Desktop/MRI_Project/Data/Best_Color/100307/T1w/T1w_acpc_dc_restore.nii.gz \
-m T1 \
-o /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/100307/T1w/T1w_acpc_dc_restore.nii.gz \
-mat /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/100307/T1w/100307_T1_

python3 Registration.py -s /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/101410/T1w/T1w_acpc_dc_restore.nii.gz \
-i /user/lcorcos/home/Desktop/MRI_Project/Data/Best_Color/101006/T1w/T1w_acpc_dc_restore.nii.gz \
-m T1 \
-o /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/101006/T1w/T1w_acpc_dc_restore.nii.gz \
-mat /user/lcorcos/home/Desktop/MRI_Project/Data/Registration/101006/T1w/101006_T1_
