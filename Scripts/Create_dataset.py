import numpy as np
import nibabel as nib
import glob
import splitfolders
from tensorflow.keras.utils import to_categorical
import sys

from sklearn.preprocessing import MinMaxScaler
scaler = MinMaxScaler()

TRAINING_DATASET_PATH = "/data/athena/user/lcorcos/HCP_Preprocess"
MASK_DATASET_PATH = '/data/athena/user/lcorcos/Auto_Genereted_HCP_Masks'
OUTPUT_NPY_DATASET = '/data/athena/user/lcorcos/BEF_DATASET'

T1_list = sorted(glob.glob(f'{TRAINING_DATASET_PATH}/Best_Color/*/T1w/T1w_acpc_dc_restore.nii.gz'))
T2_list = sorted(glob.glob(f'{TRAINING_DATASET_PATH}/Best_Color/*/T1w/T2w_acpc_dc_restore.nii.gz'))
print(len(T1_list), len(T2_list))

Mask_list = sorted(glob.glob(f'{MASK_DATASET_PATH}/*/T1w/*'))
print(len(Mask_list))

zero_mat = np.zeros((256,9,256))

for img in range(len(T2_list)): 
    print("Now preparing image and masks number: ", img)
    
    temp_image_t1=nib.load(T1_list[img]).get_fdata()
    print("Before - temp_image_t1 shap = ", temp_image_t1.shape)
    temp_image_t1=scaler.fit_transform(temp_image_t1.reshape(-1, temp_image_t1.shape[-1])).reshape(temp_image_t1.shape)
    temp_image_t1 = temp_image_t1[0:256, :, 2:258]
    temp_image_t1 = np.concatenate((temp_image_t1, zero_mat), axis=1)
    print("After - temp_image_t1 shap = ", temp_image_t1.shape, "\n")
    
    temp_image_t2=nib.load(T2_list[img]).get_fdata()
    print("Before - temp_image_t2 shap = ", temp_image_t2.shape)
    temp_image_t2=scaler.fit_transform(temp_image_t2.reshape(-1, temp_image_t2.shape[-1])).reshape(temp_image_t2.shape)
    temp_image_t2 = temp_image_t2[0:256, :, 2:258]
    temp_image_t2 = np.concatenate((temp_image_t2, zero_mat), axis=1)
    print("After - temp_image_t2 shap = ", temp_image_t2.shape, "\n")
    
    temp_mask=nib.load(Mask_list[img]).get_fdata()
    print("Before - temp_mask shap = ", temp_mask.shape)
    temp_mask=temp_mask.astype(np.uint8)
    temp_mask = temp_mask[0:256, :, 2:258]
    temp_mask = np.concatenate((temp_mask, zero_mat), axis=1)
    temp_mask= to_categorical(temp_mask, num_classes=8)
    print("After - temp_mask shap = ", temp_mask.shape, "\n---------------------------")
    
    temp_combined_images = np.stack([temp_image_t1, temp_image_t2], axis=3)
    
    np.save(f'{OUTPUT_NPY_DATASET}/NII_Files/image_'+str(img)+'.npy', temp_combined_images)
    np.save(f'{OUTPUT_NPY_DATASET}/Masks/mask_'+str(img)+'.npy', temp_mask)

# my_img=np.load('/data/athena/user/lcorcos/BEF_DATASET/NII_Files/image_0.npy')
# print(my_img.shape)
# my_mask=np.load('/data/athena/user/lcorcos/BEF_DATASET/Masks/mask_0.npy')
# print(my_mask.shape)

# input_folder = '/data/athena/user/lcorcos/BEF_DATASET'
# output_folder = '//data/athena/user/lcorcos/DATASET_HCP'

# # 70 % Train - 20 % Test - 10 % Validation 
# splitfolders.ratio(input_folder, output=output_folder, seed=20, ratio=(0.7, 0.1, 0.2), group_prefix=None, move=True)