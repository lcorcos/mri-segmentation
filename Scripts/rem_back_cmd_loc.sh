#!/bin/sh

python3 RemoveBackground.py -i /user/lcorcos/home/Desktop/MRI_Project/Data/Origin/100307/T1w/T1w_acpc_dc_restore.nii.gz -m T1 -o /user/lcorcos/home/Desktop/MRI_Project/Data/Rem_Background/100307/T1w/T1w_acpc_dc_restore.nii.gz
python3 RemoveBackground.py -i /user/lcorcos/home/Desktop/MRI_Project/Data/Origin/100307/T1w/T2w_acpc_dc_restore.nii.gz -m T2 -o /user/lcorcos/home/Desktop/MRI_Project/Data/Rem_Background/100307/T1w/T2w_acpc_dc_restore.nii.gz
python3 RemoveBackground.py -i /user/lcorcos/home/Desktop/MRI_Project/Data/Origin/101410/T1w/T1w_acpc_dc_restore.nii.gz -m T1 -o /user/lcorcos/home/Desktop/MRI_Project/Data/Rem_Background/101410/T1w/T1w_acpc_dc_restore.nii.gz
python3 RemoveBackground.py -i /user/lcorcos/home/Desktop/MRI_Project/Data/Origin/101410/T1w/T2w_acpc_dc_restore.nii.gz -m T2 -o /user/lcorcos/home/Desktop/MRI_Project/Data/Rem_Background/101410/T1w/T2w_acpc_dc_restore.nii.gz
python3 RemoveBackground.py -i /user/lcorcos/home/Desktop/MRI_Project/Data/Origin/101006/T1w/T1w_acpc_dc_restore.nii.gz -m T1 -o /user/lcorcos/home/Desktop/MRI_Project/Data/Rem_Background/101006/T1w/T1w_acpc_dc_restore.nii.gz
python3 RemoveBackground.py -i /user/lcorcos/home/Desktop/MRI_Project/Data/Origin/101006/T1w/T2w_acpc_dc_restore.nii.gz -m T2 -o /user/lcorcos/home/Desktop/MRI_Project/Data/Rem_Background/101006/T1w/T2w_acpc_dc_restore.nii.gz
python3 RemoveBackground.py -i /user/lcorcos/home/Desktop/MRI_Project/Data/Origin/100206/T1w/T1w_acpc_dc_restore.nii.gz -m T1 -o /user/lcorcos/home/Desktop/MRI_Project/Data/Rem_Background/100206/T1w/T1w_acpc_dc_restore.nii.gz
python3 RemoveBackground.py -i /user/lcorcos/home/Desktop/MRI_Project/Data/Origin/100206/T1w/T2w_acpc_dc_restore.nii.gz -m T2 -o /user/lcorcos/home/Desktop/MRI_Project/Data/Rem_Background/100206/T1w/T2w_acpc_dc_restore.nii.gz
