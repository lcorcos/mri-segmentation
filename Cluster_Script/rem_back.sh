#!/bin/bash

#OAR -l /cpu=1,walltime=00:02:00
#OAR -p cputype='xeon'
#OAR -n MRI_Remove_Background
#OAR -O /data/athena/user/lcorcos/HCP_Preprocess/Cluster_Log/Output_Rem_Back/%jobid%-stdout.txt
#OAR -E /data/athena/user/lcorcos/HCP_Preprocess/Cluster_Log/Output_Rem_Back/%jobid%-stderr.txt
#OAR --array-param-file /data/athena/user/lcorcos/HCP_Preprocess/sub-mod.txt

set -e 

date

. /home/lcorcos/miniconda3/etc/profile.d/conda.sh

conda activate lcorcosEnvs

python /home/lcorcos/Project/mri-segmentation/Scripts/RemoveBackground.py -i /data/athena/share/data/HCP/data/$1/T1w/$2w_acpc_dc_restore.nii.gz -m $2 -o /data/athena/user/lcorcos/HCP_Preprocess/Rem_Background/$1/T1w/$2w_acpc_dc_restore.nii.gz

date