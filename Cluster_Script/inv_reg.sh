#!/bin/bash

#OAR -l /cpu=1,walltime=00:01:00
#OAR -p cputype='xeon'
#OAR -n MRI_Inv_Registration
#OAR -O /data/athena/user/lcorcos/HCP_Preprocess/Cluster_Log/Inv_Reg/%jobid%-stdout.txt
#OAR -E /data/athena/user/lcorcos/HCP_Preprocess/Cluster_Log/Inv_Reg/%jobid%-stderr.txt
#OAR --array-param-file /data/athena/user/lcorcos/HCP_Preprocess/sub-mod.txt

set -e 

date

. /home/lcorcos/miniconda3/etc/profile.d/conda.sh

conda activate lcorcosEnvs

python /home/lcorcos/Project/mri-segmentation/Scripts/Inversed_Registration.py \
-s /data/athena/user/lcorcos/HCP_Preprocess/Best_Color/$1/T1w/T1w_acpc_dc_restore.nii.gz \
-i /data/athena/user/lcorcos/HCP_Preprocess/Manual_mask_101410.nii.gz \
-mat /data/athena/user/lcorcos/HCP_Preprocess/Registration/$1/T1w/$1_T1_0GenericAffine.mat \
-inv /data/athena/user/lcorcos/HCP_Preprocess/Registration/$1/T1w/$1_T1_1InverseWarp.nii.gz \
-o /data/athena/user/lcorcos/HCP_Preprocess/Inversed_Registration/$1/T1w/Manual_mask_registrated.nii.gz

date