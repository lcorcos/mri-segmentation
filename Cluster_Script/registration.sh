#!/bin/bash

#OAR -l /cpu=1,walltime=00:04:00
#OAR -p cputype='xeon'
#OAR -n MRI_Registration
#OAR -O /data/athena/user/lcorcos/HCP_Preprocess/Cluster_Log/Output_Registration_SyNRA/%jobid%-stdout.txt
#OAR -E /data/athena/user/lcorcos/HCP_Preprocess/Cluster_Log/Output_Registration_SyNRA/%jobid%-stderr.txt
#OAR --array-param-file /data/athena/user/lcorcos/HCP_Preprocess/sub-mod.txt

set -e 

date

. /home/lcorcos/miniconda3/etc/profile.d/conda.sh

conda activate lcorcosEnvs

python /home/lcorcos/Project/mri-segmentation/Scripts/Registration.py \
-s /data/athena/user/lcorcos/HCP_Preprocess/Registration/101410/T1w/$2w_acpc_dc_restore.nii.gz \
-i /data/athena/user/lcorcos/HCP_Preprocess/Best_Color/$1/T1w/$2w_acpc_dc_restore.nii.gz \
-m $2 \
-o /data/athena/user/lcorcos/HCP_Preprocess/Registration/$1/T1w/$2w_acpc_dc_restore.nii.gz \
-mat /data/athena/user/lcorcos/HCP_Preprocess/Registration/$1/T1w/$1_$2_

date