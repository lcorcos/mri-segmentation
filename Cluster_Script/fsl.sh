#!/bin/bash

#OAR -l /cpu=2,walltime=01:00:00
#OAR -p cputype='xeon'
#OAR -n MRI_FSL_T2Param
#OAR -O /data/athena/user/lcorcos/HCP_Preprocess/Cluster_Log/FSL_Test/%jobid%-stdout.txt
#OAR -E /data/athena/user/lcorcos/HCP_Preprocess/Cluster_Log/FSL_Test/%jobid%-stderr.txt
#OAR --array-param-file /data/athena/user/lcorcos/HCP_Preprocess/sub-mod-test.txt

set -e 

date

PATH=$PATH:/data/athena/share/apps/fsl/bin
export FSLDIR=/data/athena/share/apps/fsl
source ${FSLDIR}/etc/fslconf/fsl.sh

5ttgen fsl -nocrop -sgm_amyg_hipp \
/data/athena/user/lcorcos/HCP_Preprocess/Best_Color/$1/T1w/T1w_acpc_dc_restore.nii.gz \
/data/athena/user/lcorcos/HCP_Preprocess/Fast_Test/$1/T1w/mask.nii.gz \
-t2 /data/athena/user/lcorcos/HCP_Preprocess/Best_Color/$1/T1w/T2w_acpc_dc_restore.nii.gz \
-nthreads 5

date