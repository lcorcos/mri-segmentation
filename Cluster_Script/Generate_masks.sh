#!/bin/bash

#OAR -l /cpu=1,walltime=00:01:00
#OAR -p cputype='xeon'
#OAR -n MRI_Generate_Masks
#OAR -O /data/athena/user/lcorcos/HCP_Preprocess/Cluster_Log/Generate_Masks/%jobid%-stdout.txt
#OAR -E /data/athena/user/lcorcos/HCP_Preprocess/Cluster_Log/Generate_Masks/%jobid%-stderr.txt
#OAR --array-param-file /data/athena/user/lcorcos/HCP_Preprocess/sub-mod-masks.txt

set -e 

date

. /home/lcorcos/miniconda3/etc/profile.d/conda.sh

conda activate lcorcosEnvs
 
python /home/lcorcos/Project/mri-segmentation/Scripts/Semi_automated_segmentation.py \
-m /data/athena/user/lcorcos/HCP_Preprocess/Fast/$1/T1w/T1w_acpc_dc_restore.nii.gz \
-hs /data/athena/user/lcorcos/HCP_Preprocess/Inversed_Registration/$1/T1w/Manual_mask_registrated.nii.gz \
-o /data/athena/user/lcorcos/Auto_Genereted_HCP_Masks/$1/T1w/Full_Segmentation.nii.gz

date