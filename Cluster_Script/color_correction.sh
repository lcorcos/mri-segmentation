#!/bin/bash

#OAR -l /cpu=1,walltime=00:01:00
#OAR -p cputype='xeon'
#OAR -n MRI_Color_Correction
#OAR -O /data/athena/user/lcorcos/HCP_Preprocess/Cluster_Log/Output_Color_Correction/%jobid%-stdout.txt
#OAR -E /data/athena/user/lcorcos/HCP_Preprocess/Cluster_Log/Output_Color_Correction/%jobid%-stderr.txt
#OAR --array-param-file /data/athena/user/lcorcos/HCP_Preprocess/sub-mod.txt

set -e 

date

. /home/lcorcos/miniconda3/etc/profile.d/conda.sh

conda activate lcorcosEnvs

python /home/lcorcos/Project/mri-segmentation/Scripts/ImageCorrection.py -i /data/athena/user/lcorcos/HCP_Preprocess/Rem_Background/$1/T1w/$2w_acpc_dc_restore.nii.gz -m $2 -o /data/athena/user/lcorcos/HCP_Preprocess/Best_Color/$1/T1w/$2w_acpc_dc_restore.nii.gz

date
