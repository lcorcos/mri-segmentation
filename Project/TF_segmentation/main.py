import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import segmentation_models_3D as sm
import tensorflow as tf
from My_3D_Unet_model import simple_unet_model
from Datagen import *
import datetime
from keras import callbacks
# from tensorflow.keras.optimizers import Adam
from keras.optimizers import adam_v2
tf.debugging.set_log_device_placement(False)
tf.get_logger().setLevel('WARNING')
tf.autograph.set_verbosity(1)

batch_size = 3

train_img_dir = "/data/athena/user/lcorcos/DATASET_HCP_2/train/NII_Files/"
train_mask_dir = "/data/athena/user/lcorcos/DATASET_HCP_2/train/Masks/"

val_img_dir = "/data/athena/user/lcorcos/DATASET_HCP_2/test/NII_Files/"
val_mask_dir = "/data/athena/user/lcorcos/DATASET_HCP_2/test/Masks/"

train_img_list = sorted(os.listdir(train_img_dir))
train_mask_list = sorted(os.listdir(train_mask_dir))

val_img_list = sorted(os.listdir(val_img_dir))
val_mask_list = sorted(os.listdir(val_mask_dir))

print(train_img_list, "\n", train_mask_list)

train_img_datagen = imageLoader(train_img_dir, train_img_list,
                                train_mask_dir, train_mask_list, batch_size)

val_img_datagen = imageLoader(val_img_dir, val_img_list,
                              val_mask_dir, val_mask_list, batch_size)

# Verify generator
# img, msk = train_img_datagen.__next__()

# print(img.shape, "---", msk.shape)

# model = simple_unet_model(256, 320, 256, 2, 8)
# print(model.input_shape)
# print(model.output_shape)

dice_loss = sm.losses.DiceLoss()
focal_loss = sm.losses.CategoricalFocalLoss()
total_loss = dice_loss + (1 * focal_loss)

iou = sm.metrics.IOUScore(threshold=0.5)

metrics = ['accuracy', 'Precision', 'Recall', iou, sm.metrics.FScore(threshold=0.5)]

# LR = 0.0001
# optim = tf.keras.optimizers.Adam(learning_rate = 0.0001)
# optim = Adam(lr=0.0001)


optimizer = adam_v2.Adam(learning_rate=0.0001, decay=0.0001/520)

# steps_per_epoch = len(train_img_list)//batch_size
# val_steps_per_epoch = len(val_img_list)//batch_size

# print("steps_per_epoch = ", steps_per_epoch, "val_steps_per_epoch = ", val_steps_per_epoch)

tf.debugging.set_log_device_placement(False)
gpus = tf.config.list_logical_devices('GPU')
strategy = tf.distribute.MirroredStrategy(gpus)
with strategy.scope():
    model = simple_unet_model(IMG_HEIGHT=256,
                              IMG_WIDTH=320,
                              IMG_DEPTH=256,
                              IMG_CHANNELS=2,
                              num_classes=8)

    model.compile(optimizer=optimizer, loss=total_loss, metrics=metrics)
# print(model.summary())

print(model.input_shape)
print(model.output_shape)

log_dir = "Logs/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = tf.keras.callbacks.TensorBoard(
    log_dir=log_dir, histogram_freq=1, profile_batch=0)

earlystopping = callbacks.EarlyStopping(monitor ='iou_score', 
                                        mode ="max", patience = 20, 
                                        restore_best_weights = True)

checkpoint_filepath = 'home/lcorcos/Project/mri-segmentation/Project/Models'
model_checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
    filepath=checkpoint_filepath,
    save_weights_only=True,
    monitor='iou_score',
    mode='max',
    save_freq=15,
    save_best_only=True)

history = model.fit(train_img_datagen,
                    steps_per_epoch=3,
                    epochs=525,
                    verbose=1,
                    validation_data=val_img_datagen,
                    validation_steps=1,
                    callbacks=[tensorboard_callback, earlystopping, model_checkpoint_callback]
                    )

model.save('BN_3D_UNET_SEG.hdf5')


