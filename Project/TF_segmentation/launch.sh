#!/bin/bash

#OAR -t besteffort
#OAR -p gpu='YES' and host='nefgpu50.inria.fr'
#OAR -l /nodes=1/gpunum=3,walltime=20:00:00
#OAR -n UNET_3D_MRI_SEG
#OAR -O /data/athena/user/lcorcos/%jobid%-UNET_3D-stdout.txt
#OAR -E /data/athena/user/lcorcos/%jobid%-UNET_3D-stderr.txt

set -e 

date

module load singularity/3.5.2
singularity exec -B /data --nv /home/lcorcos/tensorflow_2.5.1-gpu.sif python /home/lcorcos/Project/mri-segmentation/Project/TF_segmentation/main.py

# singularity shell python3 /home/lcorcos/Project/mri-segmentation/Project/TF_segmentation/main.py

date